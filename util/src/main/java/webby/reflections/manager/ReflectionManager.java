package webby.reflections.manager;

import org.reflections.Reflections;
import org.reflections.scanners.MemberUsageScanner;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.util.Set;

public final class ReflectionManager
{
	private static final String PACKAGE_NAME = "webby";
	private static final Reflections REFLECTIONS = new Reflections(new ConfigurationBuilder().setUrls(ClasspathHelper.forPackage(PACKAGE_NAME))
																							 .setScanners(new MethodAnnotationsScanner(),
																										  new TypeAnnotationsScanner(),
																										  new SubTypesScanner(true),
																										  new MemberUsageScanner()));
	
	public static Set<Constructor> getConstructorsAnnotatedWithParticular(final Class<? extends Annotation> annotatedConstructor)
	{
		return REFLECTIONS.getConstructorsAnnotatedWith(annotatedConstructor);
	}
	
	public static Set<Class<?>> getTypesAnnotatedWithParticular(final Class<? extends Annotation> annotatedClass)
	{
		return REFLECTIONS.getTypesAnnotatedWith(annotatedClass);
	}
	
	public static Object getInstanceOfTypeAnnotatedWith(final Class<? extends Annotation> annotation, final Class<?> annotatedClass)
	{
		return null; // TODO to be changed
	}
	public static <T> Set<Class<? extends T>> getSubtypesOfParticular(Class<T> clazz)
	{
		return REFLECTIONS.getSubTypesOf(clazz);
	}
	
	public static Set<Member> getProperties(final Field field)
	{
		return REFLECTIONS.getFieldUsage(field);
	}
}
