package webby.entity;

public class User {
  private long id;
  private String name;
  private String surname;
  private int age;
  private String login;
  private String password;

  public User() {}

  public User(String name, String surname, int age, String login, String password) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.login = login;
    this.password = password;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    User user = (User) o;

    if (getAge() != user.getAge()) return false;
    if (!getName().equals(user.getName())) return false;
    return getSurname().equals(user.getSurname());
  }

  @Override
  public int hashCode() {
    int result = getName().hashCode();
    result = 31 * result + getSurname().hashCode();
    result = 31 * result + getAge();
    return result;
  }

  @Override
  public String toString() {
    return "User{"
        + "id="
        + id
        + ", name='"
        + name
        + '\''
        + ", surname='"
        + surname
        + '\''
        + ", age="
        + age
        + ", login='"
        + login
        + '\''
        + ", password='"
        + password
        + '\''
        + '}';
  }
}
