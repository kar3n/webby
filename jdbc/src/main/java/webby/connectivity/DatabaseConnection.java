package webby.connectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
  private static Connection conn;
  private static final String URL = "jdbc:mysql://localhost:3306/user_database?useSSL=false";
  private static final String USER = "root";
  private static final String PASS = "12345";

  public static Connection getConnection() {
    if (conn != null) {
      return conn;
    }
    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
      conn = DriverManager.getConnection(URL, USER, PASS);

    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    }
    return conn;
  }

  public static void closeConnection() {
    if (conn != null) {
      try {
        conn.close();
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}
