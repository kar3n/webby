create schema user_database;
use user_database;

CREATE TABLE `users`
(
    `id`       bigint       NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `name`     varchar(250) NOT NULL,
    `surname`  varchar(250) NOT NULL,
    `age`      int          NOT NULL,
    `login`    varchar(250) NOT NULL,
    `password` varchar(250) NOT NULL
) ENGINE = InnoDB;