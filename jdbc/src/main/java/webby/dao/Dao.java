package webby.dao;

import webby.entity.User;

import java.util.List;

public interface Dao {
  User create(User user);

  User getById(long id);

  List<User> getAll();

  User update(User user);

  User delete(long id);
}
