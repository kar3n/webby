package webby.dao;

import webby.connectivity.DatabaseConnection;
import webby.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao implements Dao {
  @Override
  public User create(User user) {
    try {
      PreparedStatement preparedStatement =
          DatabaseConnection.getConnection()
              .prepareStatement(
                  "INSERT INTO user_database.users(name, surname, age, login, password) VALUES (?,?,?,?,?)");
      userToStatementMapper(user, preparedStatement);
      preparedStatement.executeUpdate();
      preparedStatement.close();
    } catch (SQLException e) {
      e.printStackTrace(); // TODO log
    }
    return user;
  }

  @Override
  public User getById(long id) {
    User user = new User();
    try {
      PreparedStatement preparedStatement =
          DatabaseConnection.getConnection()
              .prepareStatement("select * from user_database.users where id = ?");
      preparedStatement.setInt(1, (int) id);
      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {
        resultSetToUserMapper(resultSet, user);
      }
      preparedStatement.close();
    } catch (SQLException e) {
      e.printStackTrace(); // TODO log
    }

    return user;
  }

  @Override
  public List<User> getAll() {
    List<User> users = new ArrayList<>();
    try {
      PreparedStatement preparedStatement =
          DatabaseConnection.getConnection().prepareStatement("select * from user_database.users");
      ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        User user = new User();
        resultSetToUserMapper(resultSet, user);
        users.add(user);
      }
      preparedStatement.close();
    } catch (Exception e) {
      e.printStackTrace(); // TODO
    }
    return users;
  }

  @Override
  public User update(User user) {
    try {
      PreparedStatement preparedStatement =
          DatabaseConnection.getConnection()
              .prepareStatement(
                  "UPDATE user_database.users set name = ?,surname = ?, age = ?, login = ?, password = ? WHERE id = ?");
      userToStatementMapper(user, preparedStatement);
      preparedStatement.setInt(6, (int) user.getId());
      preparedStatement.executeUpdate();
      preparedStatement.close();
    } catch (SQLException e) {
      e.printStackTrace(); // TODO
    }
    return user;
  }

  @Override
  public User delete(long id) {
    User user = getById(id);
    try {
      PreparedStatement preparedStatement =
          DatabaseConnection.getConnection()
              .prepareStatement("DELETE FROM user_database.users WHERE id = ?");
      preparedStatement.setInt(1, (int) id);
      preparedStatement.executeUpdate();
      preparedStatement.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return user;
  }

  private void resultSetToUserMapper(ResultSet resultSet, User user) throws SQLException {
    user.setId(resultSet.getInt("id"));
    user.setName(resultSet.getString("name"));
    user.setSurname(resultSet.getString("surname"));
    user.setAge(resultSet.getInt("age"));
    user.setLogin(resultSet.getString("login"));
    user.setPassword(resultSet.getString("password"));
  }

  private void userToStatementMapper(User user, PreparedStatement preparedStatement)
      throws SQLException {
    preparedStatement.setString(1, user.getName());
    preparedStatement.setString(2, user.getSurname());
    preparedStatement.setInt(3, user.getAge());
    preparedStatement.setString(4, user.getLogin());
    preparedStatement.setString(5, user.getPassword());
  }
}
