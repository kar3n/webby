package webby.webmodule.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProcessLogout extends HttpServlet {
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
  }
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.getSession().invalidate();
    request.setAttribute("loggedOut", "Logged Out Successfully ");
    request.getRequestDispatcher("login.jsp").forward(request, response);
  }
}
