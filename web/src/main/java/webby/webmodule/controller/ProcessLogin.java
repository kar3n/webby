package webby.webmodule.controller;

import webby.dao.Dao;
import webby.dao.UserDao;
import webby.entity.User;
import webby.webmodule.service.Service;
import webby.webmodule.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class ProcessLogin extends HttpServlet {
  
  private Service userService;
  
  public ProcessLogin() {
    super();
  }
  
  @Override
  public void init() {
    Dao userDao = new UserDao();
    userService = new UserService(userDao);
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    String login = request.getParameter("login");
    String password = request.getParameter("password");
    HttpSession session = request.getSession();
    if (login.equals("admin") && password.equals("admin")) {
      List<User> allUsers = userService.findAll();
      request.setAttribute("userList", allUsers);
      request.setAttribute("noUsers", "No registered users yet");
      request.getRequestDispatcher("adminPage.jsp").forward(request, response);
    }
    User user = new User();
    user.setLogin(login);
    user.setPassword(password);
    boolean auth = userService.authenticate(user);
    System.out.println(auth);
    
    if (auth) {
      session.setAttribute("login", login);
      request.getRequestDispatcher("homePage.jsp").forward(request, response);
    }
    else {
      
      request.setAttribute("wrongFields", "Wrong Username or Password...");
      request.getRequestDispatcher("login.jsp").forward(request, response);
    }
  }
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    if (request.getSession().getAttribute("login") == null) {
      request.setAttribute("noLogin", "Login first...");
      request.getRequestDispatcher("login.jsp").forward(request, response);
    }
  }
}
