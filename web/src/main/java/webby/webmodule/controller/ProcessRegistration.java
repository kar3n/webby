package webby.webmodule.controller;

import webby.dao.Dao;
import webby.dao.UserDao;
import webby.entity.User;
import webby.webmodule.service.Service;
import webby.webmodule.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ProcessRegistration extends HttpServlet {
  
  private Service userService;
  
  @Override
  public void init() {
    Dao userDao = new UserDao();
    userService = new UserService(userDao);
  }
  
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    String firstName = request.getParameter("first_name");
    String surname = request.getParameter("surname");
    String login = request.getParameter("login");
    String password = request.getParameter("password");
    String confPassword = request.getParameter("confirmPassword");
    String age = request.getParameter("age");
    List<String> params = new ArrayList<>();
    params.add(firstName.trim());
    params.add(surname.trim());
    params.add(login.trim());
    params.add(password.trim());
    params.add(age.trim());
    if (age.isBlank() || Integer.parseInt(age) <= 0) {
      request.setAttribute("ageError", "Invalid user age...");
      request.getRequestDispatcher("registration.jsp").forward(request, response);
    }
    boolean errorField = false;
    for (String check : params) {
      if (check.isBlank()) {
        errorField = true;
        break;
      }
    }
    if (errorField) {
      request.setAttribute("error", "Invalid fields...");
      request.getRequestDispatcher("registration.jsp").forward(request, response);
    }
    User user = new User();
    user.setName(firstName);
    user.setSurname(surname);
    user.setLogin(login);
    user.setPassword(password);
    user.setAge(Integer.parseInt(age));
    boolean isExist = userService.isExist(user);
    if (!password.equals(confPassword)) {
      request.setAttribute("mismatch", "Passwords does not match");
      request.getRequestDispatcher("registration.jsp").forward(request, response);
    }
    else if (isExist) {
      request.setAttribute("exist", "Login is already taken");
      request.getRequestDispatcher("registration.jsp").forward(request, response);
    }
    else {
      userService.save(user);
      request.setAttribute("successMessage", "User Registered successfully");
      request.getRequestDispatcher("login.jsp").forward(request, response);
    }
  }
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
  }
}
