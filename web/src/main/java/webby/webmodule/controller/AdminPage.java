package webby.webmodule.controller;

import webby.dao.UserDao;
import webby.webmodule.service.Service;
import webby.webmodule.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminPage extends HttpServlet {
	
	private Service userService;
	
	@Override
	public void init() {
		userService = new UserService(new UserDao());
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("userList", userService.findAll());
		request.getRequestDispatcher("admin.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}
