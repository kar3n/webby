package webby.webmodule.service;

import webby.dao.Dao;
import webby.entity.User;

import java.util.List;

public class UserService implements Service {
  private Dao userDao;

  public UserService(Dao userDao) {
    this.userDao = userDao;
  }

  @Override
  public User save(User user) {
    if (userDao.create(user) != null) {
      return user;
    }
    return null;
  }

  @Override
  public User findById(long id) {
    return userDao.getById(id);
  }

  @Override
  public List<User> findAll() {
    return userDao.getAll();
  }

  @Override
  public User update(User user) {
    return userDao.update(user);
  }

  @Override
  public User delete(long id) {
    return userDao.delete(id);
  }

  @Override
  public boolean isExist(User user) {
    boolean isExist = false;
    List<User> searchingList = userDao.getAll();
    for (User tempUser : searchingList) {
      if (tempUser.getLogin().equals(user.getLogin())) {
        isExist = true;
        break;
      }
    }
    return isExist;
  }

  @Override
  public boolean authenticate(User user) {
    boolean authenticate = false;
    List<User> searchingList = userDao.getAll();
    String login = user.getLogin();
    String password = user.getPassword();
    for (User tempUser : searchingList) {
      if (login.equals(tempUser.getLogin()) && password.equals(tempUser.getPassword())) {
        authenticate = true;
        break;
      }
    }
    return authenticate;
  }
}
