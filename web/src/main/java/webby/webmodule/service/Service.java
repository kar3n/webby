package webby.webmodule.service;

import webby.entity.User;

import java.util.List;

public interface Service {
  User save(User user);

  User findById(long id);

  List<User> findAll();

  User update(User user);

  User delete(long id);

  boolean isExist(User user);

  boolean authenticate(User user);
}
