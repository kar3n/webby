<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: karenho
  Date: 10/10/2019
  Time: 2:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<%--<%request.getAttribute()%>--%>
<div align="center">
    <form action="/login" method="post">
        <table class="formTable">
            <tr>
                <td>UserName</td>
                <td><input type="text" name="login"/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password"/></td>
            </tr>

        </table>
        <input type="submit" value="Submit"/>
    </form>
    Don't have an account? <a href="registration.jsp">Register</a><br>
    <p style="color: #00ce1a;font-family: 'Fira Code', sans-serif; font-size: 12pt"><%if (request.getAttribute("successMessage") != null)%><%=request
            .getAttribute("successMessage")%>
    </p>
    <p style="color: #00ce1a;font-family: 'Fira Code', sans-serif; font-size: 12pt"><%if (request.getAttribute("loggedOut") != null)%><%=request
            .getAttribute("loggedOut")%>
    </p>
    <p style="color: #ce0406;font-family: 'Fira Code', sans-serif; font-size: 12pt"><%if (request.getAttribute("noLogin") != null)%><%=request
            .getAttribute("noLogin")%>
    </p>
    <p style="color: #ce170c;font-family: 'Fira Code', sans-serif; font-size: 12pt"><%if (request.getAttribute("wrongFields") != null)%><%=request
            .getAttribute("wrongFields")%>
    </p>
</div>

</body>
</html>
