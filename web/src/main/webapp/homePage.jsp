<%--
  Created by IntelliJ IDEA.
  User: karenho
  Date: 10/10/2019
  Time: 4:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<html>
<head>
    <title>Home Page</title>
</head>
<body>
<div align="center">
    <h1 style="color: brown">Home Page</h1>
    <p style="font-family: 'Fira Code', sans-serif;font-size: 10pt;color: brown">
        <%if (session.getAttribute("login") != null){%>
        Welcome: <%=session.getAttribute("login")%>
        <%}else {%>
        <%response.sendRedirect("/login");%>
        <%}%>
    </p><br>
    <a href="/logout">Logout</a>
</div>


</body>
</html>
