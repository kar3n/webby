<%@ page import="java.io.PrintWriter" %><%--
  Created by IntelliJ IDEA.
  User: karenho
  Date: 10/10/2019
  Time: 12:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Registration</title>
    <link rel="stylesheet" type="text/css" href="resources/style.css">

</head>
<body>
<div align="center">
    <p style="color: #ff1016;font-family: 'Fira Code', sans-serif; font-size: 10pt"><%if (request.getAttribute("error") != null)%><%=request
            .getAttribute("error")%>
    </p>
    <p style="color: #ff1016;font-family: 'Fira Code', sans-serif; font-size: 10pt"><%if (request.getAttribute("exist") != null)%><%=request
            .getAttribute("exist")%>
    </p>
    <p style="color: #ff1016;font-family: 'Fira Code', sans-serif; font-size: 10pt"><%if (request.getAttribute("mismatch") != null)%><%=request
            .getAttribute("mismatch")%>
    </p>
    <p style="color: #ff1016;font-family: 'Fira Code', sans-serif; font-size: 10pt"><%if (request.getAttribute("ageError") != null)%><%=request
            .getAttribute("ageError")%>
    </p>
    <form action="/register" method="post">
        <table class="formTable">
            <tr>
                <td>First Name</td>
                <td><input type="text" name="first_name"/></td>
            </tr>
            <tr>
                <td>Surname</td>
                <td><input type="text" name="surname"/></td>
            </tr>
            <tr>
                <td>Login</td>
                <td><input type="text" name="login"/></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" name="password"/></td>
            </tr>
            <tr>
                <td>Confirm Password</td>
                <td><input type="password" name="confirmPassword"/></td>
            </tr>
            <tr>
                <td>Age</td>
                <td><input type="number" name="age"/></td>
            </tr>
        </table>
        <input type="submit" value="Submit"/></form>
</div>


</body>
</html>
