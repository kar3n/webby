<%@ page import="webby.entity.User" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: karenho
  Date: 10/10/2019
  Time: 4:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Admin</title>
</head>
<body>

<div align="center">
    <table border="2">
        <thead>
        <tr>
            <th>#</th>
            <th>User Name</th>
            <th>Password</th>
            <th>Email</th>
            <th>Display Name</th>
        </tr>
        </thead>
        <tbody>
        <%
            int i = 1;
            List<User> list = (List) request.getAttribute("userList");
        %>
        <%if (list == null || list.size() == 0){%>
        <%=request.getAttribute("noUsers")%>
        <%}else{%>
        <%
            for (User u : list) {
        %>
        <tr>
            <td><%=i++%></td>
            <td><%=u.getName()%></td>
            <td><%=u.getSurname()%></td>
            <td><%=u.getLogin()%></td>
            <td><%=u.getAge()%></td>
        </tr>
        <%
            }
        %>
        <%
            }
        %>
        </tbody>
    </table>
</div>
<div align="center">
    <a href="/logout">Logout</a>
</div>

</body>
</html>
